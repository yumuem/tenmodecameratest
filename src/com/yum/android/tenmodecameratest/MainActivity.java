package com.yum.android.tenmodecameratest;



import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements CallbackListener{
	
	protected static final String LOG_TAG = MainActivity.class.getName();
	// RF select dialog 
	private int mResult = -1;
	private int mSelectedItem = -1;

	private TenModeCountdownTimer mCountDownTimer;
	private Timer mResumeTimer = null;
	private static PowerManager mPowerManager;
	private static PowerManager.WakeLock mWakeLock;
	
	private ImageButton mShutterButton;
	private TextView mCountdownTextView;
	private TextView mCycleTextView;
	private TextView mStepTextView;	
	private TextView mShootingCountTextView;
	private SeekBar mZoomSeekBar;
	
	public final static int INTERVAL_CAPTUER_MILLISEC = 30* 1000;
	public final static int INTERVAL_SHOWCOUNTDOWN_MILLISEC = 1000;
	public final static int NUM_CAPTURE_PER_ONE_CYCLE = 10;
	public final static int INTERVAL_SLEEP_SEC_MIN = 10;
	public final static int INTERVAL_SLEEP_SEC_MAX = 600;
	public final static int INTERVAL_SLEEP_SEC_STEP = 10;
	
	private final static int NUM_TEMPORARY_ZOOM_STEP = 100;
	private Integer mCycle = 1;
	private Integer mStep  = 1;
	private Integer mShootingCount = 0;
	private Integer mSleepMillisec = 10 * 1000;
	
	private CameraPreview mCameraPreview;
	
	static final int RESULT_ENABLE = 1;
	private ComponentName mCN;
	private DevicePolicyManager mDPM;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mCameraPreview = new CameraPreview(this);
		setContentView(mCameraPreview);
		
		//setContentView(R.layout.activity_main);
        final RelativeLayout controlOverlayView = (RelativeLayout)this.getLayoutInflater().inflate(R.layout.activity_main, null);
        this.addContentView(controlOverlayView,
                new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
        // Screen always ON
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);    
        
        // Set Countdown timer
        mCountDownTimer = new TenModeCountdownTimer(INTERVAL_CAPTUER_MILLISEC, INTERVAL_SHOWCOUNTDOWN_MILLISEC );
        mCountdownTextView = (TextView)findViewById(R.id.textView_currentcountdown);
        mCountDownTimer.setTextView(mCountdownTextView);

        
        mCycleTextView = (TextView)findViewById(R.id.textView_currentcycle);
        mCycleTextView.setText(mCycle.toString());

        mStepTextView = (TextView)findViewById(R.id.textView_currentstep);
        mStepTextView.setText(mStep.toString());

        mShootingCountTextView = (TextView)findViewById(R.id.textView_currentshootingcount);
        
        mZoomSeekBar = (SeekBar)findViewById(R.id.seekBar_Zoom);
        mZoomSeekBar.setProgress(0);   
        mZoomSeekBar.setMax(NUM_TEMPORARY_ZOOM_STEP);
        
        
        mDPM = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
        mCN = new ComponentName(this, TenModeDeviceAdminReceiver.class);

        if (!mDPM.isAdminActive(mCN)) {
            // Register device admin
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);  
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mCN);
            startActivityForResult(intent, RESULT_ENABLE);
        }
        
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,   
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD); 
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    switch (requestCode) {
	        case RESULT_ENABLE:
	            if (resultCode == Activity.RESULT_OK) {
	                // Has become the device administrator.
	                Log.w(LOG_TAG, "Administration enabled!");
	            }
	            else {
	                //Canceled or failed.
	                Log.w(LOG_TAG, "Administration enable FAILED!");
	            }
	            return;
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		case R.id.reset_setting:
			mCycle = 1;
	        mCycleTextView.setText(mCycle.toString());
			Toast.makeText(this, "Reset Cycle", Toast.LENGTH_SHORT );
			return true;
		case R.id.total_reset_setting:
			// Remove all files			
			File[] files=this.getExternalFilesDir(null).listFiles();
			for(int i=0; i<files.length; i++){
				if (files[i].delete()){
					Log.w(LOG_TAG, "file delete complete:"+files[i].toString());
				}
				else{
					Log.w(LOG_TAG, "file delete error!:"+files[i].toString());
				}
			}
		
			mShootingCount = 0;
			mShootingCountTextView.setText(mShootingCount.toString());
			Toast.makeText(this, "Remove all files and Reset Shooting count", Toast.LENGTH_SHORT ).show();
			return true;
		case R.id.rf_settings:
			createRFSelectDialog().show();		
			return true;
		case R.id.sleeptime_setting:
			createSleepTimeSelectDialog().show();
			return true;
		case R.id.admin_setting:
			Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);  
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mCN);
            startActivityForResult(intent, RESULT_ENABLE);
			return true;
		}		
		return false;
	}
	
	@Override
	public void onResume(){
		super.onResume();
		Log.w(LOG_TAG, "Set & Start Countdown callback");
		mStep=1;
		mStepTextView.setText(mStep.toString());
		mCycleTextView.setText(mCycle.toString());	// update the cycle
		mCountDownTimer.setCallbackListner(this);
        mCountDownTimer.start();
	}
	
	@Override
	public void onPause(){
		super.onPause();
		Log.w(LOG_TAG, "Cancel & Remove Countdown callback");
		mCountDownTimer.cancel();
		mCountDownTimer.removeCallbackListener();		
	}
	
	public Dialog createRFSelectDialog() {
		mSelectedItem = -1;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

	    builder.setTitle(R.string.rf_settings)
	           .setSingleChoiceItems(R.array.rf_settings_array, mResult, 
	                      new DialogInterface.OnClickListener() {
	        	   @Override
	               public void onClick(DialogInterface dialog, int which) {
	        		   mSelectedItem = which;
	               }
	           })
	    // Set the action buttons
	           .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	                   // User clicked OK, so save the mSelectedItems results somewhere
	                   // or return them to the component that opened the dialog
	            	   mResult = mSelectedItem;
	            	   if (mResult == -1){
	            		   return;
	            	   }
	            	   // Set the 3G/WiFi (GPS&WiFi)
	            	   String[] rf_settings = getResources().getStringArray(R.array.rf_settings_array);
	            	   Resources res = getApplicationContext().getResources();
	            	  
	            	   if(rf_settings[mResult].equals(res.getString(R.string.rf_off))){
	            		   setRFAvailable(false);
	            	   }
	            	   else if (rf_settings[mResult].equals(res.getString(R.string.rf_on))){
	            		   setRFAvailable(true);
	            	   }
	            	   else if (rf_settings[mResult].equals(res.getString(R.string.rf_active_only))){
	            		   setRFAvailable(true);
	            	   }	            	   
	               }
	           })
	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	            	   // Do nothing
	               }
	           });

	    return builder.create();
	}
	
	public Dialog createSleepTimeSelectDialog(){
		mSelectedItem = -1;
		
        RelativeLayout linearLayout = new RelativeLayout(this);
        final NumberPicker numberPicker = new NumberPicker(this);
        numberPicker.setMaxValue(INTERVAL_SLEEP_SEC_MAX/INTERVAL_SLEEP_SEC_STEP); //600sec/
        numberPicker.setMinValue(INTERVAL_SLEEP_SEC_MIN/INTERVAL_SLEEP_SEC_STEP); //10sec.
        //numberPicker.setValue(mSleepMillisec/(1000*INTERVAL_SLEEP_SEC_STEP));
        int step = INTERVAL_SLEEP_SEC_STEP;
        String[] valueSet = new String[INTERVAL_SLEEP_SEC_MAX/INTERVAL_SLEEP_SEC_MIN];
        for (int i = INTERVAL_SLEEP_SEC_MIN; i <= INTERVAL_SLEEP_SEC_MAX; i += step) {
            valueSet[(i/step)-1] = String.valueOf(i);
        }
        numberPicker.setDisplayedValues(valueSet);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        linearLayout.addView(numberPicker,numPicerParams);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Integer sleepIntervalSec = mSleepMillisec/1000;
        builder.setTitle("Select Sleep time(sec.) / Current:" + sleepIntervalSec.toString() + "s.");
        builder.setView(linearLayout);
        builder.setCancelable(false)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                Log.e("","New Quantity Value : "+ numberPicker.getValue());
                                mSleepMillisec = numberPicker.getValue() *INTERVAL_SLEEP_SEC_STEP* 1000;
                            }
                        })
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
	    return builder.create();		
	}

	public void setRFAvailable(boolean isrfon){
		if( isrfon == false ){
 		   Log.d(LOG_TAG, "Set Airplane mode");
 		   // all wifi setting off of airplane mode
/*            Settings.Global.putInt(
                    getApplicationContext().getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 1);
 		   Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
 		   intent.putExtra("state", true);
 		   sendBroadcast(intent);			
*/
	        //Wifi-setting on
	        WifiManager wifi = (WifiManager) getSystemService(WIFI_SERVICE);
	        wifi.setWifiEnabled(false); 		   
		}
		else{
/*
 	        Log.d(LOG_TAG, "Disabled Airplane mode");
            Settings.Global.putInt(
            		getApplicationContext().getContentResolver(),
            		Settings.Global.AIRPLANE_MODE_ON, 0);
		    Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
		    intent.putExtra("state", false);
		    sendBroadcast(intent);	            	        
*/
		   // Connection condition
		    ConnectivityManager connMgr = (ConnectivityManager) 
		    getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI); 
			boolean isWifiConn = networkInfo.isConnected();
			networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			boolean isMobileConn = networkInfo.isConnected();
			Log.d(LOG_TAG, "Wifi connected: " + isWifiConn);
			Log.d(LOG_TAG, "Mobile connected: " + isMobileConn);   		            		   
    			
	        //Wifi-setting on
	        WifiManager wifi = (WifiManager) getSystemService(WIFI_SERVICE);
	        wifi.setWifiEnabled(true);
		}
		
	}
	
	@Override
	public void callback(TenModeCountdownTimer t) {
		Log.w(LOG_TAG, "Countdown callback");
		//AutoFocus();
		mCameraPreview.Shutter(mStep);
		mShootingCount++;
		mShootingCountTextView.setText(mShootingCount.toString());
		
		if(mStep < NUM_CAPTURE_PER_ONE_CYCLE){
			mStep++;
			mStepTextView.setText(mStep.toString());
			mCountDownTimer.start();
			if( mStep %2 == 0 ){	// Even number is Tele shoot
		        mZoomSeekBar.setProgress(NUM_TEMPORARY_ZOOM_STEP); 
			}
	  		else{	// Odd number is Wide shoot
		        mZoomSeekBar.setProgress(0); 	  		
	  		}
			
		}
		else{
			Log.w(LOG_TAG, "Cancel & Remove Countdown callback");
			mCountDownTimer.cancel();
			mCountDownTimer.removeCallbackListener();

//			mPowerManager = (PowerManager) getSystemService(this.getApplicationContext().POWER_SERVICE);

	        if (!mDPM.isAdminActive(mCN)) {
	            // Register device admin
	            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);  
	            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mCN);
	            startActivityForResult(intent, RESULT_ENABLE);
	        }
	        else {
	        	Intent intent = new Intent(MainActivity.this, AlarmReceiver.class);  
	        	PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);  
	        	
	        	Calendar calendar = Calendar.getInstance();  
	        	calendar.setTimeInMillis(System.currentTimeMillis());  
	        	calendar.add(Calendar.MILLISECOND, mSleepMillisec);  

	        	AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);  // one shot  
	        	alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);  
	        	
	        	mCycle++;
	            // if already registed, execute lockscreen
	        	Log.w(LOG_TAG, "lock Now:"+ mSleepMillisec.toString()+"msec.");
	        	mDPM.lockNow();
	        }
/*
	        if(mResumeTimer == null){	            	 
                mResumeTimer = new Timer(true);
            }               
                mResumeTimer.schedule(new TimerTask(){
                    @Override
                    public void run() {
                    	//Log.w(LOG_TAG, "Wake up");
                    	Log.w(LOG_TAG, "Full wake lock");
                    	//get up from Sleep
                    	mWakeLock = mPowerManager
//                    			.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK
                            			.newWakeLock(PowerManager.FULL_WAKE_LOCK
//                                    			.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                    					| PowerManager.ACQUIRE_CAUSES_WAKEUP
                    					| PowerManager.ON_AFTER_RELEASE, "disableLock");
//                    	mWakeLock = mPowerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "TenModeTimerWakeLock");
                    	mWakeLock.acquire();
                    	mWakeLock.release();
                		mCycle++;
                		//mCycleTextView.setText(mCycle.toString()); //crashing because of cannot attach the textview 	
                    }
                }, mSleepMillisec);
//            }			 
 */
		}		
	}
	
}
