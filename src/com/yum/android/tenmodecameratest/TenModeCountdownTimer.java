package com.yum.android.tenmodecameratest;

import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;




public class TenModeCountdownTimer extends CountDownTimer {
    private final String TAG = "CountDownTimer";
    private TextView mCountDownTextView;
    private CallbackListener mCallbackListener = null;
    
    public void setCallbackListner(CallbackListener c){
    	mCallbackListener = c;
    }

    public void removeCallbackListener(){
    	mCallbackListener = null;
    }
    
	public TenModeCountdownTimer(long millisInFuture, long countDownInterval) {
		super(millisInFuture, countDownInterval);
	}
	
	public void setTextView(TextView t){
		mCountDownTextView = t;
	}

	@Override
	public void onFinish() {
		Log.w(TAG, "onFinish");	
		mCountDownTextView.setText("Done");
		if (mCallbackListener != null){
			mCallbackListener.callback(this);
		}
	}
	
	@Override
	public void onTick(long millisUntilFinished) {
		Double countdownsec = (double)(millisUntilFinished/1000);
		mCountDownTextView.setText(countdownsec.toString());
	}

	
}
