package com.yum.android.tenmodecameratest;

import java.util.Iterator;
import java.util.List;

import android.hardware.Camera;
import android.hardware.Camera.Size;

public class CameraSettingUtil {
	private static Camera mCamera;
	private static Camera.Parameters mCameraParams;
	
	private static CameraSettingUtil mCameraSettingUtil = new CameraSettingUtil();
	
	private CameraSettingUtil(){
	}
	
	public static CameraSettingUtil getInstance(){
		return mCameraSettingUtil;
	}
	
	public void setCamera(Camera cam){
		mCamera = cam;
	}
	public void releaseCamera(){
		mCamera = null;
	}

	// Set the Max Image SIze
	public void setMaxImageSize(){
		if (mCamera != null){
			List<Camera.Size> sizes = mCameraParams.getSupportedPictureSizes();
			int maxheight = 0;
			int index = 0;			
			for (int i = 0, n = sizes.size(); i < n; i++) {
				Camera.Size s = sizes.get(i);
			    if (s.height > maxheight){
			    	maxheight = s.height;
			    	index = i;
			    }
			}
			mCameraParams.setPictureSize(sizes.get(index).width, sizes.get(index).height);
		}
	}

	
	// Set the W-limit for start
	public void setWideZoom(){
		if (mCamera != null){
			int currentZoomPosition = mCameraParams.getZoom();
			if( currentZoomPosition > 0 ){
				mCameraParams.setZoom(0);
			}
		}
	}
	// Set the T-limit 
	public void setTeleZoom(){
		if (mCamera != null){
			int currentZoomPosition = mCameraParams.getZoom();
			if( currentZoomPosition < mCameraParams.getMaxZoom() ){
				mCameraParams.setZoom(mCameraParams.getMaxZoom());
			}
		}
	}
	
	// Get the Zoom step 
	public int getNumberOfZoomStep(){
		if (mCamera != null){
			return (mCameraParams.getMaxZoom()+1) ;
		}
		return 0;
	}
	
	// Set the Flash  
	public void setFlashForceOn(){
		if (mCamera != null){
			List<String> supportedFlashModes = mCameraParams.getSupportedFlashModes();
			if( supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_ON) ){
				mCameraParams.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
			}
		}
	}
	 
	public void setFlashOff(){
		if (mCamera != null){
			mCameraParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
		}
	}

	public void getParametersFromCamera(){
		if (mCamera != null){
			mCameraParams = mCamera.getParameters();
		}	
	}
	public void setParametersToCamera(){
		if (mCamera != null){
			mCamera.setParameters(mCameraParams);
		}	
	}
	
}
