
package com.yum.android.tenmodecameratest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, PreviewCallback{
	
	private final static String SAVE_FOLDER_NAME = "TenModeCameraTest";
	private static final String LOG_TAG = CameraPreview.class.getName();;
    private Activity mActivity;
    private Context mContext;
    private SurfaceHolder mHolder;
    private Camera mCamera;
    CameraSettingUtil mCameraSettingUtil;
    
    public CameraPreview(Context context){
    	super(context);
    	mContext = context;
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mCameraSettingUtil = CameraSettingUtil.getInstance();
    }
    
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (null == mCamera) {
            mCamera = Camera.open();
            mCameraSettingUtil.setCamera(mCamera);
            
            mCameraSettingUtil.getParametersFromCamera();
            // Set Max image size 
            mCameraSettingUtil.setMaxImageSize();
  			mCameraSettingUtil.setWideZoom();
  			mCameraSettingUtil.setFlashOff();
  			mCameraSettingUtil.setParametersToCamera();
        }
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (IOException e) {
            mCamera.release();
            mCamera = null;
            mCameraSettingUtil.releaseCamera();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mCamera.stopPreview();

        Log.v(LOG_TAG, "Desired Preview Size - w: " + width + ", h: " + height);

        mCamera.stopPreview();
        Parameters params = mCamera.getParameters();
        /*params.setPreviewSize(width, height);*/

        Size sz = params.getSupportedPreviewSizes().get(0);
        params.setPreviewSize(sz.width, sz.height);

        mCamera.setParameters(params);
        mCamera.startPreview();

        mCamera.setPreviewCallback(this);	
/*        
        Parameters mParam = mCamera.getParameters();
    	

        // Set orientation
        boolean portrait = isPortrait();
        // 2.2 and later

        if (portrait) {
            mCamera.setDisplayOrientation(90);
        } else {
            mCamera.setDisplayOrientation(0);
        }

        // Set width & height
        int previewWidth = width;
        int previewHeight = height;
        // Meaning of width and height is switched for preview when portrait,
        // while it is the same as user's view for surface and metrics.
        // That is, width must always be larger than height for setPreviewSize.

        if (portrait) {
            previewWidth = height;
            previewHeight = width;
        }

        // Actual preview size will be one of the sizes obtained by getSupportedPreviewSize.
        // It is the one that has the largest width among the sizes of which both width and
        // height no larger than given size in setPreviewSize.
        List<Size> sizes = mParam.getSupportedPreviewSizes();
        int tmpHeight = 0;
        int tmpWidth = 0;
        for (Size size : sizes) {
            if ((size.width > previewWidth) || (size.height > previewHeight)) {
                continue;
            }
            if (tmpHeight < size.height) {
                tmpWidth = size.width;
                tmpHeight = size.height;
            }
        }
        previewWidth = tmpWidth;
        previewHeight = tmpHeight;
        
        // Only for debugging
        for (Size size : sizes) {
            Log.v(LOG_TAG, "w: " + size.width + ", h: " + size.height);
        }
       
        
        mParam.setPreviewSize(previewWidth, previewHeight);

        Log.v(LOG_TAG, "Preview Actual Size - w: " + previewWidth + ", h: " + previewHeight);

        // Adjust SurfaceView size
        float layoutHeight, layoutWidth;
        if (portrait) {
            layoutHeight = previewWidth;
            layoutWidth = previewHeight;
        } else {
            layoutHeight = previewHeight;
            layoutWidth = previewWidth;
        }

        float factH, factW, fact;
        factH = height / layoutHeight;
        factW = width / layoutWidth;
        // Select smaller factor, because the surface cannot be set to the size larger than display metrics.
        if (factH < factW) {
            fact = factH;
        } else {
            fact = factW;
        }
        ViewGroup.LayoutParams layoutParams = this.getLayoutParams();
        layoutParams.height = (int)(layoutHeight * fact);
        layoutParams.width = (int)(layoutWidth * fact);
        this.setLayoutParams(layoutParams);

        Log.v(LOG_TAG, "Preview Layout Size - w: " + layoutParams.width + ", h: " + layoutParams.height);
        
        mCamera.setParameters(mParam);
        mCamera.startPreview();
*/
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (null == mCamera) {
            return;
        }
        mCamera.stopPreview();
        mCamera.setPreviewCallback(null); // Need to avoid the camera release error(Method called after release():setHasPreviewCallback)
        mCamera.release();
        mCamera = null;
        mCameraSettingUtil.releaseCamera();
    }

    protected boolean isPortrait() {
//        return (mActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
        return (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }
    
    public void Shutter(final int nthshutter) {
  	  mCamera.takePicture(
          new ShutterCallback() {
            public void onShutter() {}
          },
          null, new PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
		      	Uri uri;
		      	//saveImagetoFile(data);
		      	
		      	uri = saveImagetoURI(data);
		      	  //if(uri != null){
		          //	File file = new File(uri.getPath());
		          //	mCaptureImageView.setImageURI(Uri.parse(file.toString()));  
		      	  //}
		        camera.startPreview();
				// Camera setting
		        mCameraSettingUtil.getParametersFromCamera();
		        if( nthshutter %2 == 0 ){
		  			mCameraSettingUtil.setWideZoom();
		  			mCameraSettingUtil.setFlashOff();
		  		}
		  		else{	// Next time, 2n-th shuuting is Tele & Force Flash
		  			mCameraSettingUtil.setTeleZoom();
		  			mCameraSettingUtil.setFlashForceOn();
		  		}
		  		mCameraSettingUtil.setParametersToCamera();              
		    }
          }
        );
    }

    public void saveImagetoFile(byte[] data){
        try {

            FileOutputStream fos = new FileOutputStream(getCapturedFileName());
            fos.write(data);
            fos.close();

          } catch (FileNotFoundException e) {
            e.printStackTrace();
          } catch (IOException e) {
            e.printStackTrace();
          }
    }
    
    private String getCapturedFileName(){
    	Date date = new Date();
    	// Set the format
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmss");
    	//String path = Environment.getExternalStorageDirectory().getPath();
    	//File dir = new File (path + "/" +SAVE_FOLDER_NAME);
    	File dir = mContext.getExternalFilesDir(null);
    	//Create Folder if it's not exists
        if (!dir.exists ()){
          dir.mkdir ();
        }
        String fname = dir.getPath() + "/" + sdf.format(new Date()) + ".jpg";
    	return fname;
    }

    public Uri saveImagetoURI(byte[] data){
  	  Uri uri = null;
  	  
      if (data == null)
      {
        Log.e (LOG_TAG, "Empty data");
        mCamera.startPreview ();
        return uri;
      }
  	  Bitmap tmp_bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
      Log.i (LOG_TAG, "bitmap size is "+ ((Integer)data.length).toString());
  	  
  	  int height = tmp_bitmap.getHeight();
  	  int width = tmp_bitmap.getWidth();
  	  
//  	  Bitmap resizedBitmap = BitmapUtility.resizeBitmapToDisplaySize(height, width, bitmap);
//  	  mCaptureImageView.setImageBitmap(resizedBitmap);	  
  	  
      String filename = getCapturedFileName();
  	  Bitmap bitmap = Bitmap.createBitmap (tmp_bitmap, 0, 0, width, height, null, true);
      OutputStream out;
		try {
			Log.i(LOG_TAG, "file name:"+filename);
			out = new FileOutputStream (filename);
	        bitmap.compress (CompressFormat.JPEG, 100, out);
	        out.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		long date = System.currentTimeMillis ();
        try
        {
          //Regist Contents
          ContentValues values = new ContentValues ();
          ContentResolver contentResolver = mContext.getContentResolver ();
          values.put (Images.Media.MIME_TYPE, "image/jpeg");
          values.put (Images.Media.DATA, filename);
          values.put (Images.Media.SIZE, new File (filename).length ());
          values.put (Images.Media.DATE_ADDED, date);
          values.put (Images.Media.DATE_TAKEN, date);
          values.put (Images.Media.DATE_MODIFIED, date);
          values.put (Images.Media.HEIGHT, height);
          values.put (Images.Media.WIDTH,width );

          contentResolver.insert (MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        }
        catch (Exception e)
        {
          Log.d ("TAG", "Recognize this image after reboot");
          e.printStackTrace ();
        }

  	  return uri;
    }

	@Override
	public void onPreviewFrame(byte[] arg0, Camera arg1) {
		// TODO Auto-generated method stub
		
	}

}
