package com.yum.android.tenmodecameratest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {
	protected static final String LOG_TAG =  AlarmReceiver.class.getName();
	private static PowerManager mPowerManager;
	private static PowerManager.WakeLock mWakeLock;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.w(LOG_TAG, "Alarm Received! : " + intent.getIntExtra(Intent.EXTRA_ALARM_COUNT, 0)); 
		
    	Log.w(LOG_TAG, "Full wake lock");
    	//get up from Sleep
    	
		mPowerManager = (PowerManager)context.getSystemService( Context.POWER_SERVICE);

    	mWakeLock = mPowerManager
//    			.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK
//            			.newWakeLock(PowerManager.FULL_WAKE_LOCK
                    			.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
    					| PowerManager.ACQUIRE_CAUSES_WAKEUP
    					| PowerManager.ON_AFTER_RELEASE, "disableLock");
//    	mWakeLock = mPowerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "TenModeTimerWakeLock");
    	mWakeLock.acquire(10000);
//    	mWakeLock.release();
	}

}
